package winmaster.mindwavewatcher.Helpers;

import com.github.pwittchen.neurosky.library.message.enums.Signal;
import java.text.SimpleDateFormat;
import java.time.LocalDateTime;
import java.util.Date;
import java.util.List;
import java.util.Locale;
import java.util.OptionalDouble;

public class Helpers
{
    public static String getFormattedMessage(String messageFormat, Signal signal)
    {
        return String.format(Locale.getDefault(), messageFormat, signal.getValue());
    }

    public static String getFormattedDurationTime(long time)
    {
        int seconds = (int) (time / 1000);
        int minutes = seconds / 60;
        seconds     = seconds % 60;
        return  String.format(Locale.getDefault(),"%d:%02d", minutes, seconds);
    }

    public static String getCurrentTimeStampString()
    {
        return new SimpleDateFormat("yyy-MM-dd HH:mm:ss", Locale.getDefault()).format(new Date().getTime());
    }

    public static String getCurrentTimeStampShortString()
    {
        LocalDateTime time = LocalDateTime.now();
        return String.format(Locale.getDefault(),"%d_%d_%d_%d_%d_%d_", time.getYear(), time.getMonthValue(), time.getDayOfMonth(), time.getHour(), time.getMinute(), time.getSecond());
    }

    public static double getAverageFromListInt(List<Integer> list)
    {
        OptionalDouble result = list.stream()
                .mapToDouble(a -> a)
                .average();

        return result.isPresent() ? result.getAsDouble() : 0;
    }

    public static double getTruncatedPercentageFromProbability(float probability){
        return Math.floor(probability * 10000 ) / 100;
    }
}
