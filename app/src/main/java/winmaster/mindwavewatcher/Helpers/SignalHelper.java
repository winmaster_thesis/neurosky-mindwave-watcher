package winmaster.mindwavewatcher.Helpers;

import com.github.pwittchen.neurosky.library.message.enums.Signal;
import winmaster.mindwavewatcher.Enums.SignalStrength;
import winmaster.mindwavewatcher.R;

public class SignalHelper
{
    private static SignalStrength getSignalStrength(Signal signal)
    {
        int computedValue = (int)(-0.5 * signal.getValue() + 100);
        if(computedValue >= 75) return SignalStrength.GOOD;
        if(computedValue < 25) return SignalStrength.BAD;
        else return SignalStrength.MEDIUM;
    }

    public static int signalPercentageConverter(int value, int maxThreshold)
    {
        return (int)((double)value/maxThreshold * 100);
    }

    public static int getSignalStrengthDrawableId(Signal signal)
    {
        SignalStrength signalStrength = getSignalStrength(signal);
        switch (signalStrength)
        {
            case BAD: {
                return R.drawable.no_signal;
            }
            case GOOD:{
                return R.drawable.connected;
            }
            case MEDIUM:{
                return R.drawable.check;
            }
            default:{
                return R.drawable.headset;
            }
        }
    }
}
