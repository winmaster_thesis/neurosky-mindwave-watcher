package winmaster.mindwavewatcher.Helpers;

import android.app.NotificationChannel;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.Context;
import android.os.Build;
import android.support.v4.app.NotificationCompat;
import android.support.v4.app.NotificationManagerCompat;
import winmaster.mindwavewatcher.R;

public class NotificationHelper
{
    public static void createNotificationChannel(Context ctx)
    {
        // Create the NotificationChannel, but only on API 26+ because
        // the NotificationChannel class is new and not in the support library
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
            CharSequence name = ctx.getString(R.string.app_name);
            String description = ctx.getString(R.string.app_name);
            int importance = NotificationManager.IMPORTANCE_DEFAULT;
            NotificationChannel channel = new NotificationChannel(ctx.getResources().getString(R.string.CHANNEL_ID), name, importance);
            channel.setDescription(description);
            // Register the channel with the system; you can't change the importance
            // or other notification behaviors after this
            NotificationManager notificationManager = ctx.getSystemService(NotificationManager.class);
            notificationManager.createNotificationChannel(channel);
        }
    }

    public static void showNotification(Context context, String contentTitle, String contentText, PendingIntent intent, int notificationId){
        NotificationCompat.Builder builder = new NotificationCompat.Builder(context, context.getResources().getString(R.string.CHANNEL_ID))
                .setSmallIcon(R.drawable.mindwavewatcher)
                .setContentTitle(contentTitle)
                .setContentText(contentText)
                .setContentIntent(intent)
                .setPriority(NotificationCompat.PRIORITY_DEFAULT);

        NotificationManagerCompat.from(context).notify(notificationId, builder.build());
    }
}
