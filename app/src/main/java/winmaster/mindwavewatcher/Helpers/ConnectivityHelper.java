package winmaster.mindwavewatcher.Helpers;

import android.bluetooth.BluetoothAdapter;
import android.content.Context;
import android.net.ConnectivityManager;
import android.widget.Toast;
import com.github.pwittchen.neurosky.library.message.enums.State;
import winmaster.mindwavewatcher.Logic.MindWaveLogic.MindWaveService;
import winmaster.mindwavewatcher.R;

public class ConnectivityHelper
{
    public static boolean isNetworkConnected(Context ctx)
    {
        ConnectivityManager connectivityManager = (ConnectivityManager) ctx.getSystemService(Context.CONNECTIVITY_SERVICE);
        return connectivityManager != null && connectivityManager.getActiveNetworkInfo() != null && connectivityManager.getActiveNetworkInfo().isConnected();
    }

    public static boolean isBluetoothConnected()
    {
        BluetoothAdapter bluetoothAdapter = BluetoothAdapter.getDefaultAdapter();
        return bluetoothAdapter.isEnabled();
    }

    public static boolean isMindWaveConnected()
    {
        return MindWaveService.mindWaveState.getValue() == State.CONNECTED;
    }

    public static boolean isDevicePreparedForTests(Context ctx)
    {
        if(!isBluetoothConnected())
        {
            Toast.makeText(ctx, ctx.getResources().getString(R.string.connectBluetooth), Toast.LENGTH_SHORT).show();
        }
        if(!isMindWaveConnected())
        {
            Toast.makeText(ctx, ctx.getResources().getString(R.string.connectHeadset), Toast.LENGTH_SHORT).show();
        }
        if(!isNetworkConnected(ctx))
        {
            Toast.makeText(ctx, ctx.getResources().getString(R.string.connectInternet), Toast.LENGTH_SHORT).show();
        }

        return isBluetoothConnected() && isMindWaveConnected() && isNetworkConnected(ctx);
    }
}
