package winmaster.mindwavewatcher.Helpers;

import android.graphics.Color;
import java.util.ArrayList;
import java.util.List;
import lecho.lib.hellocharts.model.Column;
import lecho.lib.hellocharts.model.SubcolumnValue;

public class ChartHelper
{
    public static final int COLOR_DELTA = Color.parseColor("#BC0022");
    public static final int COLOR_THETA = Color.parseColor("#782FEF");
    public static final int COLOR_LOW_ALPHA = Color.parseColor("#FBFF00");
    public static final int COLOR_HIGH_ALPHA = Color.parseColor("#EAE114");
    public static final int COLOR_LOW_BETA = Color.parseColor("#5BFF62");
    public static final int COLOR_HIGH_BETA = Color.parseColor("#41B619");
    public static final int COLOR_LOW_GAMA = Color.parseColor("#1EC9E8");
    public static final int COLOR_MID_GAMA = Color.parseColor("#1771F1");

    public static final int[] COLORS = new int[]{COLOR_DELTA, COLOR_THETA, COLOR_LOW_ALPHA, COLOR_HIGH_ALPHA,COLOR_LOW_BETA, COLOR_HIGH_BETA, COLOR_LOW_GAMA, COLOR_MID_GAMA};

    public static Column setChartColumn(int data, int type)
    {
        List<SubcolumnValue> value = new ArrayList<>();
        value.add(new SubcolumnValue(data, COLORS[type-1]));
        Column column = new Column(value);
        column.setHasLabels(true);
        return column;
    }
}
