package winmaster.mindwavewatcher.Activities;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.TextView;

import pl.droidsonroids.gif.GifImageView;
import winmaster.mindwavewatcher.Helpers.ConnectivityHelper;
import winmaster.mindwavewatcher.R;
import winmaster.mindwavewatcher.ViewModels.TestTutorialViewModel;

public class TestExplanation extends AppCompatActivity
{
    String testActivator;
    TestTutorialViewModel testTutorialViewModel;

    TextView textViewTestTitle;
    TextView textViewBackgroundStory;
    TextView textViewTestPurpose;
    TextView textViewHowToBehave;
    GifImageView gifImageViewExplanation;

    @Override
    protected void onCreate(Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_test_explanation);

        textViewTestTitle = findViewById(R.id.textViewTestTitle);
        textViewBackgroundStory = findViewById(R.id.textViewBackgroundStory);
        textViewTestPurpose = findViewById(R.id.textViewTestPurpose);
        textViewHowToBehave = findViewById(R.id.textViewHowToBehave);
        gifImageViewExplanation = findViewById(R.id.gifImageViewExplanation);

        testActivator = getIntent().getExtras().getString(getResources().getString(R.string.ACTIVATOR));

        testTutorialViewModel = new TestTutorialViewModel(getApplicationContext(), testActivator);

        textViewTestTitle.setText(testTutorialViewModel.getTestTitle());
        textViewBackgroundStory.setText(testTutorialViewModel.getBackgroundStory());
        textViewTestPurpose.setText(testTutorialViewModel.getTestPurpose());
        textViewHowToBehave.setText(testTutorialViewModel.getTestHowToBehave());
        gifImageViewExplanation.setImageResource(testTutorialViewModel.getTestExplanation());

    }

    public void buttonLetsStartClicked(View view)
    {
        if(ConnectivityHelper.isDevicePreparedForTests(this))
        {
           startActivity(testTutorialViewModel.getTestIntent());
        }
    }
}
