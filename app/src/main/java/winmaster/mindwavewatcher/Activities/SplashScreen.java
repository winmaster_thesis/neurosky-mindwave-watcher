package winmaster.mindwavewatcher.Activities;

import android.content.Intent;
import android.os.Handler;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.ImageView;
import winmaster.mindwavewatcher.R;

public class SplashScreen extends AppCompatActivity
{
    @Override
    protected void onCreate(Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_splash_screen);

        ImageView imageViewLogo = findViewById(R.id.imageViewLogo);
        ImageView imageViewGearSmall = findViewById(R.id.imageViewGearSmall);
        ImageView imageViewGearBig = findViewById(R.id.imageViewGearBig);

        Animation iconAnimation = AnimationUtils.loadAnimation(this, R.anim.splash_screen);
        Animation gearAnimation = AnimationUtils.loadAnimation(this, R.anim.rotation_fade);
        imageViewLogo.startAnimation(iconAnimation);
        imageViewGearBig.startAnimation(gearAnimation);
        imageViewGearSmall.startAnimation(gearAnimation);

        new Handler().postDelayed(() -> startActivity(new Intent(SplashScreen.this, MainActivity.class)), getResources().getInteger(R.integer.splashScreenLength));
    }
}
