package winmaster.mindwavewatcher.Activities.LoginActivities;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.ImageView;
import android.widget.Toast;
import com.squareup.picasso.Picasso;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Iterator;
import java.util.List;
import winmaster.mindwavewatcher.Enums.LoginTestVariants;
import winmaster.mindwavewatcher.R;

public class LoginPictureChooser extends AppCompatActivity
{
    ImageView imageViewFirst;
    ImageView imageViewSecond;
    ImageView imageViewThird;
    ImageView imageViewFourth;
    int padding;
    boolean isPictureValid;
    String testVariant;
    String userName;

    List<String> photoUrls = new ArrayList<>();
    List<ImageView> imageViews = new ArrayList<>();

    @Override
    protected void onCreate(Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login_picture_chooser);
        padding = getResources().getInteger(R.integer.picturePadding);

        testVariant = getIntent().getExtras().getString(getResources().getString(R.string.VARIANT));
        userName = getIntent().getExtras().getString(getResources().getString(R.string.USER));

        imageViewFirst = findViewById(R.id.imageViewFirst);
        imageViewSecond = findViewById(R.id.imageViewSecond);
        imageViewThird = findViewById(R.id.imageViewThird);
        imageViewFourth = findViewById(R.id.imageViewFourth);

        imageViews.add(imageViewFirst);
        imageViews.add(imageViewSecond);
        imageViews.add(imageViewThird);
        imageViews.add(imageViewFourth);

        getPhotoUrlsAndShuffle();
        arrangePhotosUrlsToImageViews();
    }

    @Override
    public void onBackPressed() {
        Toast.makeText(this, getResources().getString(R.string.onBackPressedBlocker), Toast.LENGTH_SHORT).show();
    }


    public void buttonLoginSubmitClicked(View view)
    {
        if(!checkIfPictureWasChosen()){
            Toast.makeText(this, getResources().getString(R.string.pictureFormValidation), Toast.LENGTH_SHORT).show();
        }
        else
        {
            if(testVariant.equals(LoginTestVariants.VariantB.name()))
            {
                if(!isPictureValid)
                {
                    Toast.makeText(this, getResources().getString(R.string.pictureChooserTestBValid), Toast.LENGTH_SHORT).show();
                    startActivity(new Intent(this, LoginPicturePeeker.class));
                    return;
                }
            }
            startActivity(new Intent(this, LoginResult.class)
                    .putExtra(getResources().getString(R.string.VARIANT), testVariant)
                    .putExtra(getResources().getString(R.string.USER), userName)
                    .putExtra(getResources().getString(R.string.PICTURE_VALID), isPictureValid));
        }
    }

    public void imageClicked(View view){
        resetPadding();
        //beautiful :D
        view.setPadding(padding,padding,padding,padding);
        isPictureValid = view.isSelected();
    }

    private boolean checkIfPictureWasChosen(){
        for (ImageView imageView: imageViews) {
            if(imageView.getPaddingTop() == padding){
                return true;
            }
        }
        return false;
    }

    private void resetPadding(){
        for (ImageView imageView: imageViews) {
            imageView.setPadding(0,0,0,0);
        }
    }

    private void getPhotoUrlsAndShuffle(){
        photoUrls.add(getResources().getString(R.string.fakeUserPicture1));
        photoUrls.add(getResources().getString(R.string.fakeUserPicture2));
        photoUrls.add(getResources().getString(R.string.fakeUserPicture3));
        photoUrls.add(getResources().getString(R.string.correctUserPicture));

        Collections.shuffle(photoUrls);
    }

    private void arrangePhotosUrlsToImageViews()
    {
        Iterator<String> photoUrlsIterator = photoUrls.iterator();
        for (ImageView imageView: imageViews)
        {
            String url = photoUrlsIterator.next();
            Picasso.get().load(url).into(imageView);
            if(url.equals(getResources().getString(R.string.correctUserPicture))){
                imageView.setSelected(true);
            }
        }
    }
}
