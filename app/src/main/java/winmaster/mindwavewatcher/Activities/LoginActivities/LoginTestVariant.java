package winmaster.mindwavewatcher.Activities.LoginActivities;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import winmaster.mindwavewatcher.Enums.LoginTestVariants;
import winmaster.mindwavewatcher.R;

public class LoginTestVariant extends AppCompatActivity
{

    @Override
    protected void onCreate(Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login_test_variant);
    }

    public void buttonVariantAClicked(View view)
    {
        startActivity(new Intent(this, LoginForm.class).putExtra(getResources().getString(R.string.VARIANT),LoginTestVariants.VariantA.name()));
    }

    public void buttonVariantBClicked(View view)
    {
        startActivity(new Intent(this, LoginPicturePeeker.class).putExtra(getResources().getString(R.string.VARIANT),LoginTestVariants.VariantB.name()));
    }
}
