package winmaster.mindwavewatcher.Activities.LoginActivities;

import android.content.Intent;
import android.os.AsyncTask;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.view.animation.AnimationUtils;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;
import com.google.gson.Gson;
import com.google.gson.stream.JsonReader;
import com.squareup.picasso.Picasso;
import com.squareup.picasso.PicassoTools;
import java.io.DataOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.URL;
import winmaster.mindwavewatcher.Activities.MainActivity;
import winmaster.mindwavewatcher.Enums.LoginResultsStatuses;
import winmaster.mindwavewatcher.Enums.LoginTestVariants;
import winmaster.mindwavewatcher.Helpers.Helpers;
import winmaster.mindwavewatcher.Logic.MindWaveLogic.MindWaveDataMapper;
import winmaster.mindwavewatcher.Logic.SettingsLogic.SettingsService;
import winmaster.mindwavewatcher.Models.API.EEGPrediction;
import winmaster.mindwavewatcher.Models.API.MindWaveDTO;
import winmaster.mindwavewatcher.R;
import winmaster.mindwavewatcher.ViewModels.LoginResultViewModel;

public class LoginResult extends AppCompatActivity
{
    LoginResultViewModel loginResultViewModel;

    boolean isPictureValid;
    boolean isPredictionResultReliable;
    String testVariant;
    String userName;

    //ML.NET API results
    boolean verified;
    double percentage;
    LoginResultsStatuses status;

    TextView textViewTestVariant;
    TextView textViewPictureMatched;
    TextView textViewLoginConfidence;
    TextView textViewVerified;
    TextView textViewPercentage;
    ImageView imageViewLoginResult;

    LinearLayout layoutLoading;
    ImageView imageViewFidgetLoading;

    @Override
    protected void onCreate(Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login_result);

        testVariant = getIntent().getExtras().getString(getResources().getString(R.string.VARIANT));
        userName = getIntent().getExtras().getString(getResources().getString(R.string.USER));
        isPictureValid = getIntent().getExtras().getBoolean(getResources().getString(R.string.PICTURE_VALID));

        layoutLoading = findViewById(R.id.layoutLoading);
        imageViewFidgetLoading = findViewById(R.id.imageViewFidgetLoading);
        textViewTestVariant = findViewById(R.id.textViewTestVariant);
        textViewPictureMatched = findViewById(R.id.textViewPictureMatched);
        textViewLoginConfidence = findViewById(R.id.textViewLoginConfidence);
        textViewVerified = findViewById(R.id.textViewVerified);
        textViewPercentage = findViewById(R.id.textViewPercentage);
        imageViewLoginResult = findViewById(R.id.imageViewLoginResult);

        imageViewFidgetLoading.startAnimation(AnimationUtils.loadAnimation(this, R.anim.fidget_spinning));

        //settings part
        setSettingsPartForUI();

        loginResultViewModel = new LoginResultViewModel(this);
        MindWaveDTO dto = MindWaveDataMapper.prepareDataFromEEG();

        //API request for login prediction
        AsyncTaskGetResultFromAPI getResultFromAPI = new AsyncTaskGetResultFromAPI();
        getResultFromAPI.execute(dto);
    }

    @Override
    public void onBackPressed()
    {
        PicassoTools.clearCache(Picasso.get());
        startActivity(new Intent(this, MainActivity.class));
    }

    public void buttonLoginResultFinishClicked(View view)
    {
        onBackPressed();
    }

    private void setSettingsPartForUI()
    {
        textViewTestVariant.setText(testVariant);
        textViewPictureMatched.setText(isPictureValid ? getResources().getString(R.string.Yes) : getResources().getString(R.string.No));
        textViewPictureMatched.setTextColor(isPictureValid ? getColor(R.color.colorPrimary) : getColor(R.color.colorAccent));
        textViewLoginConfidence.setText(String.valueOf(SettingsService.getLoginConfidenceLevel(this) + "%"));
    }

    private void setLoginResultOnUI()
    {
        textViewVerified.setText(verified ? getResources().getString(R.string.Yes) : getResources().getString(R.string.No));
        textViewVerified.setTextColor(verified ? getColor(R.color.colorPrimary) : getColor(R.color.colorAccent));
        textViewPercentage.setText(String.format("%s%%", String.valueOf(percentage)));
        textViewPercentage.setTextColor(isPredictionResultReliable ? getColor(R.color.colorPrimary) : getColor(R.color.colorAccent));

        if(testVariant.equals(LoginTestVariants.VariantA.name()))
        {
            if(verified && isPictureValid){
                status = LoginResultsStatuses.HACKERMAN;
                imageViewLoginResult.setImageDrawable(getDrawable(R.drawable.hackerman));
            }
            else {
                status = LoginResultsStatuses.NOT_PASSED;
                imageViewLoginResult.setImageDrawable(getDrawable(R.drawable.not_passed));
            }
        }
        else
        {
            imageViewLoginResult.setImageDrawable((verified && isPredictionResultReliable) ? getDrawable(R.drawable.passed) : getDrawable(R.drawable.not_passed));
            status = (verified && isPredictionResultReliable) ? LoginResultsStatuses.PASSED : LoginResultsStatuses.NOT_PASSED;
        }
    }

    private class AsyncTaskGetResultFromAPI extends AsyncTask<MindWaveDTO, Void, EEGPrediction>
    {
        @Override
        protected void onPreExecute()
        {
            super.onPreExecute();
        }

        @Override
        protected EEGPrediction doInBackground(MindWaveDTO... dto)
        {
            Gson gson = new Gson();
            EEGPrediction result = null;
            try
            {
                URL url = new URL(getApplicationContext().getResources().getString(R.string.API_URL));
                HttpURLConnection urlConnection = (HttpURLConnection) url.openConnection();
                urlConnection.setRequestMethod("POST");
                urlConnection.setRequestProperty("Content-Type", "application/json");
                urlConnection.setRequestProperty("Accept","application/json");
                urlConnection.setDoOutput(true);
                urlConnection.setDoInput(true);
                urlConnection.connect();

                String jsonString = gson.toJson(dto[0]);
                DataOutputStream outputStream = new DataOutputStream(urlConnection.getOutputStream());
                outputStream.writeBytes(jsonString);
                outputStream.flush();
                outputStream.close();

                if (urlConnection.getResponseCode() == getResources().getInteger(R.integer.apiCodeOK))
                {
                    InputStream responseBody = urlConnection.getInputStream();
                    InputStreamReader responseBodyReader = new InputStreamReader(responseBody);
                    JsonReader jsonReader = new JsonReader(responseBodyReader);
                    result = gson.fromJson(jsonReader, EEGPrediction.class);
                }

                urlConnection.disconnect();
                Thread.sleep(getResources().getInteger(R.integer.delayForAnimation));
            }
            catch (IOException | InterruptedException e)
            {
                e.printStackTrace();
            }

            return result;
        }

        @Override
        protected void onPostExecute(EEGPrediction result)
        {
            super.onPostExecute(result);

            if(result != null)
            {
                percentage = Helpers.getTruncatedPercentageFromProbability(result.probability);
                verified = result.prediction;
                isPredictionResultReliable = percentage >= SettingsService.getLoginConfidenceLevel(getApplicationContext());

                //set picture and other things after verification
                setLoginResultOnUI();
                layoutLoading.setVisibility(View.GONE);

                //save results to DB
                loginResultViewModel.saveLoginResultDataToDb(userName, testVariant, isPictureValid, verified, percentage, status.name());
            }
            else{
                Toast.makeText(getApplicationContext(), getApplicationContext().getResources().getString(R.string.somethingWrong), Toast.LENGTH_SHORT).show();
                onBackPressed();
            }
        }
    }
}
