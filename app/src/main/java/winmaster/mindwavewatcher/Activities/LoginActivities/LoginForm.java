package winmaster.mindwavewatcher.Activities.LoginActivities;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;
import winmaster.mindwavewatcher.R;

public class LoginForm extends AppCompatActivity
{
    TextView textViewValidation;
    EditText editTextUsername;
    EditText editTextPassword;
    String testVariant;

    @Override
    protected void onCreate(Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login_form);

        testVariant = getIntent().getExtras().getString(getResources().getString(R.string.VARIANT));
        editTextUsername = findViewById(R.id.editTextUsername);
        editTextPassword = findViewById(R.id.editTextPassword);
        textViewValidation = findViewById(R.id.textViewValidation);
    }

    public void loginButtonClicked(View view)
    {
        if(loginValid() && passwordValid()){
            startActivity(new Intent(this, LoginPictureChooser.class)
                    .putExtra(getResources().getString(R.string.VARIANT), testVariant)
                    .putExtra(getResources().getString(R.string.USER), editTextUsername.getText().toString()));
        }
        else {
            String validationMessage="";
            if(!passwordValid()){
                validationMessage = getResources().getString(R.string.passwordFormValidation);
            }
            if(!loginValid()){
                validationMessage+=getResources().getString(R.string.loginFormValidation);
            }
            textViewValidation.setText(validationMessage);
        }
    }

    @Override
    public void onBackPressed() {
        Toast.makeText(this, getResources().getString(R.string.onBackPressedBlocker), Toast.LENGTH_SHORT).show();
    }


    private boolean passwordValid(){
        return editTextPassword.getText().toString().equals(getResources().getString(R.string.passwordHint));
    }

    private boolean loginValid(){
        return !editTextUsername.getText().toString().equals("");
    }
}
