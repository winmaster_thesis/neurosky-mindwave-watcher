package winmaster.mindwavewatcher.Activities.LoginActivities;

import android.content.Intent;
import android.os.CountDownTimer;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.Toast;
import com.squareup.picasso.Picasso;
import java.util.Locale;
import winmaster.mindwavewatcher.Enums.LoginTestVariants;
import winmaster.mindwavewatcher.R;

public class LoginPicturePeeker extends AppCompatActivity
{
    ImageView imageViewPicturePeek;
    Button buttonNext;

    @Override
    protected void onCreate(Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login_picture_peeker);

        imageViewPicturePeek = findViewById(R.id.imageViewPicturePeek);
        buttonNext = findViewById(R.id.buttonNext);
        buttonNext.setEnabled(false);
        Picasso.get().load(getResources().getString(R.string.correctUserPicture)).into(imageViewPicturePeek);

        new CountDownTimer(getResources().getInteger(R.integer.countDownPicture), getResources().getInteger(R.integer.countDownPictureInterval)){
            @Override
            public void onTick(long millisToWait) {
                int timeToWait = (int)millisToWait/getResources().getInteger(R.integer.countDownPictureInterval);
                buttonNext.setText(String.format(Locale.getDefault(),"%s (%d)", getResources().getString(R.string.nextButton), timeToWait));
            }

            @Override
            public void onFinish() {
                buttonNext.setText(getResources().getString(R.string.nextButton));
                buttonNext.setEnabled(true);
                buttonNext.setBackgroundColor(getColor(R.color.colorPrimary));
            }
        }.start();
    }

    @Override
    public void onBackPressed() {
        Toast.makeText(this, getResources().getString(R.string.onBackPressedBlocker), Toast.LENGTH_SHORT).show();
    }

    public void buttonNextClicked(View view)
    {
        startActivity(new Intent(this, LoginForm.class).putExtra(getResources().getString(R.string.VARIANT),LoginTestVariants.VariantB.name()));
    }
}
