package winmaster.mindwavewatcher.Activities;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.EditText;
import android.widget.RadioButton;
import android.widget.TextView;
import android.widget.Toast;
import winmaster.mindwavewatcher.Enums.SettingsType;
import winmaster.mindwavewatcher.Helpers.SharedPreferencesUtils;
import winmaster.mindwavewatcher.R;

public class Settings extends AppCompatActivity
{
    RadioButton radioButtonTestsEasy,
            radioButtonTestsMedium,
            radioButtonTestsHard,
            radioButtonTestsCustom;

    TextView textViewMeditationValueEasy, textViewAttentionValueEasy,
            textViewMeditationValueMedium, textViewAttentionValueMedium,
            textViewMeditationValueHard, textViewAttentionValueHard;

    private SettingsType currentSettingChecked;

    EditText editTextAttentionValueCustom, editTextMeditationValueCustom, editTextLoginConfidence;

    @Override
    protected void onCreate(Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_settings);

        radioButtonTestsEasy = findViewById(R.id.radioButtonTestsEasy);
        radioButtonTestsMedium = findViewById(R.id.radioButtonTestsMedium);
        radioButtonTestsHard = findViewById(R.id.radioButtonTestsHard);
        radioButtonTestsCustom = findViewById(R.id.radioButtonTestsCustom);

        textViewMeditationValueEasy = findViewById(R.id.textViewMeditationValueEasy);
        textViewMeditationValueEasy.setText(String.format("%d%%", getResources().getInteger(R.integer.testsEasyMeditationValue)));

        textViewAttentionValueEasy = findViewById(R.id.textViewAttentionValueEasy);
        textViewAttentionValueEasy.setText(String.format("%d%%", getResources().getInteger(R.integer.testsEasyAttentionValue)));

        textViewMeditationValueMedium = findViewById(R.id.textViewMeditationValueMedium);
        textViewMeditationValueMedium.setText(String.format("%d%%", getResources().getInteger(R.integer.testsMediumMeditationValue)));

        textViewAttentionValueMedium = findViewById(R.id.textViewAttentionValueMedium);
        textViewAttentionValueMedium.setText(String.format("%d%%", getResources().getInteger(R.integer.testsMediumAttentionValue)));

        textViewMeditationValueHard = findViewById(R.id.textViewMeditationValueHard);
        textViewMeditationValueHard.setText(String.format("%d%%", getResources().getInteger(R.integer.testsHardMeditationValue)));

        textViewAttentionValueHard = findViewById(R.id.textViewAttentionValueHard);
        textViewAttentionValueHard.setText(String.format("%d%%", getResources().getInteger(R.integer.testsHardAttentionValue)));

        editTextLoginConfidence = findViewById(R.id.editTextLoginConfidence);

        editTextAttentionValueCustom = findViewById(R.id.editTextAttentionValueCustom);
        editTextMeditationValueCustom = findViewById(R.id.editTextMeditationValueCustom);

        String savedSetting = SharedPreferencesUtils.readSharedStringSetting(this, getString(R.string.SETTINGS_TYPE), SettingsType.MEDIUM.name());
        setupSettings(savedSetting);

        if(!savedSetting.equals(SettingsType.CUSTOM.name())){
            editTextAttentionValueCustom.setEnabled(false);
            editTextMeditationValueCustom.setEnabled(false);
        }

        radioButtonTestsEasy.setOnClickListener(view -> {
            radioButtonTestsEasy.setChecked(true);
            radioButtonTestsMedium.setChecked(false);
            radioButtonTestsHard.setChecked(false);
            radioButtonTestsCustom.setChecked(false);
            editTextAttentionValueCustom.setEnabled(false);
            editTextMeditationValueCustom.setEnabled(false);

            currentSettingChecked = SettingsType.EASY;
        });

        radioButtonTestsMedium.setOnClickListener(view -> {
            radioButtonTestsEasy.setChecked(false);
            radioButtonTestsMedium.setChecked(true);
            radioButtonTestsHard.setChecked(false);
            radioButtonTestsCustom.setChecked(false);
            editTextAttentionValueCustom.setEnabled(false);
            editTextMeditationValueCustom.setEnabled(false);

            currentSettingChecked = SettingsType.MEDIUM;
        });

        radioButtonTestsHard.setOnClickListener(view -> {
            radioButtonTestsEasy.setChecked(false);
            radioButtonTestsMedium.setChecked(false);
            radioButtonTestsHard.setChecked(true);
            radioButtonTestsCustom.setChecked(false);
            editTextAttentionValueCustom.setEnabled(false);
            editTextMeditationValueCustom.setEnabled(false);

            currentSettingChecked = SettingsType.HARD;
        });

        radioButtonTestsCustom.setOnClickListener(view -> {
            radioButtonTestsEasy.setChecked(false);
            radioButtonTestsMedium.setChecked(false);
            radioButtonTestsHard.setChecked(false);
            radioButtonTestsCustom.setChecked(true);
            editTextAttentionValueCustom.setEnabled(true);
            editTextMeditationValueCustom.setEnabled(true);

            currentSettingChecked = SettingsType.CUSTOM;
        });
    }

    private void setupSettings(String setting)
    {
        switch (setting)
        {
            case "EASY":
            {
                radioButtonTestsEasy.setChecked(true);
                currentSettingChecked = SettingsType.EASY;
                break;
            }
            case "MEDIUM":
            {
                radioButtonTestsMedium.setChecked(true);
                currentSettingChecked = SettingsType.MEDIUM;
                break;
            }
            case "HARD":
            {
                radioButtonTestsHard.setChecked(true);
                currentSettingChecked = SettingsType.HARD;
                break;
            }
            case "CUSTOM":
            {
                //If there is no proper value, use default medium setup
                int customAttention = SharedPreferencesUtils.readSharedIntSetting(this, "SETTING_CUSTOM_ATTENTION", getResources().getInteger(R.integer.testsMediumAttentionValue));
                int customMeditation = SharedPreferencesUtils.readSharedIntSetting(this, "SETTING_CUSTOM_MEDITATION", getResources().getInteger(R.integer.testsMediumMeditationValue));

                radioButtonTestsCustom.setChecked(true);
                editTextAttentionValueCustom.setText(String.valueOf(customAttention));
                editTextMeditationValueCustom.setText(String.valueOf(customMeditation));
                currentSettingChecked = SettingsType.CUSTOM;
                break;
            }
        }

        int loginConfidence = SharedPreferencesUtils.readSharedIntSetting(this, "SETTING_CUSTOM_LOGIN_CONFIDENCE", getResources().getInteger(R.integer.loginConfidenceLevelDefault));
        editTextLoginConfidence.setText(String.valueOf(loginConfidence));
    }

    public void buttonSaveClicked(View view)
    {
        if(validateEnteredValues())
        {
            switch (currentSettingChecked)
            {
                case EASY:
                {
                    SharedPreferencesUtils.saveSharedStringSetting(this, getResources().getString(R.string.SETTINGS_TYPE), SettingsType.EASY.name());
                    break;
                }
                case MEDIUM:
                {
                    SharedPreferencesUtils.saveSharedStringSetting(this, getResources().getString(R.string.SETTINGS_TYPE), SettingsType.MEDIUM.name());
                    break;
                }
                case HARD:
                {
                    SharedPreferencesUtils.saveSharedStringSetting(this, getResources().getString(R.string.SETTINGS_TYPE), SettingsType.HARD.name());
                    break;
                }
                case CUSTOM:
                {
                    SharedPreferencesUtils.saveSharedStringSetting(this, getResources().getString(R.string.SETTINGS_TYPE), SettingsType.CUSTOM.name());

                    int attention = Integer.parseInt(editTextAttentionValueCustom.getText().toString());
                    int meditation = Integer.parseInt(editTextMeditationValueCustom.getText().toString());

                    SharedPreferencesUtils.saveSharedIntSetting(this, getResources().getString(R.string.SETTING_CUSTOM_ATTENTION), attention);
                    SharedPreferencesUtils.saveSharedIntSetting(this, getResources().getString(R.string.SETTING_CUSTOM_MEDITATION), meditation);
                    break;
                }
            }

            int loginConfidence = Integer.parseInt(editTextLoginConfidence.getText().toString());
            if(loginConfidence != getResources().getInteger(R.integer.loginConfidenceLevelDefault)){
                SharedPreferencesUtils.saveSharedIntSetting(this, getResources().getString(R.string.SETTING_CUSTOM_LOGIN_CONFIDENCE), loginConfidence);
            }

            Toast.makeText(getApplicationContext(), getResources().getString(R.string.saveSucceeded), Toast.LENGTH_SHORT).show();
            onBackPressed();
        }
        else Toast.makeText(getApplicationContext(), getResources().getString(R.string.settingsValidation), Toast.LENGTH_LONG).show();
    }

    private boolean validateEnteredValues()
    {
        if(radioButtonTestsCustom.isChecked())
        {
            if(editTextMeditationValueCustom.getText().toString().equals("") || editTextAttentionValueCustom.getText().toString().equals(""))
                return false;

            int attention = Integer.parseInt(editTextAttentionValueCustom.getText().toString());
            int meditation = Integer.parseInt(editTextMeditationValueCustom.getText().toString());

            if(attention > 100 || meditation > 100 || attention < 10 || meditation < 10)
            {
                return false;
            }
        }

        if(editTextLoginConfidence.getText().toString().equals("")) return false;
        int loginConfidence = Integer.parseInt(editTextLoginConfidence.getText().toString());

        return loginConfidence <= 100 && loginConfidence >= 10;
    }
}
