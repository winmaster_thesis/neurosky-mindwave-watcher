package winmaster.mindwavewatcher.Activities;

import android.content.Intent;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.os.Bundle;
import com.github.paolorotolo.appintro.AppIntro2;
import com.github.paolorotolo.appintro.AppIntroFragment;
import winmaster.mindwavewatcher.R;

public class OnboardingTutorial extends AppIntro2
{
    @Override
    protected void onCreate(Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);

        addSlide(AppIntroFragment.newInstance(getResources().getText(R.string.app_name),
                getResources().getText(R.string.tutorialDescription),
                R.drawable.thinking,
                getColor(R.color.colorAccent)));
        addSlide(AppIntroFragment.newInstance(getResources().getText(R.string.slideMindWaveTitle),
                getResources().getText(R.string.slideMindWaveDescription),
                R.drawable.mindwave_diagram,
                getColor(R.color.colorPrimaryDark)));
        addSlide(AppIntroFragment.newInstance(getResources().getText(R.string.slideLoginTitle),
                getResources().getText(R.string.slideLoginDescription),
                R.drawable.onboarding_password,
                getColor(R.color.confettiColor5)));
        addSlide(AppIntroFragment.newInstance(getResources().getText(R.string.slideAutomationTitle),
                getResources().getText(R.string.slideAutomationDescription),
                R.drawable.onboarding_tasks,
                getColor(R.color.colorPrimary)));
        addSlide(AppIntroFragment.newInstance(getResources().getText(R.string.slideMindOverMatterTitle),
                getResources().getText(R.string.slideMindOverMatterDescription),
                R.drawable.onboarding_react,
                getColor(R.color.confettiColor4)));
        setFadeAnimation();
    }
    @Override
    public void onSkipPressed(Fragment currentFragment)
    {
        super.onSkipPressed(currentFragment);
        startActivity(new Intent(this, MainActivity.class));
    }

    @Override
    public void onDonePressed(Fragment currentFragment)
    {
        super.onDonePressed(currentFragment);
        startActivity(new Intent(this, MainActivity.class));
    }

    @Override
    public void onSlideChanged(@Nullable Fragment oldFragment, @Nullable Fragment newFragment)
    {
        super.onSlideChanged(oldFragment, newFragment);
    }
}
