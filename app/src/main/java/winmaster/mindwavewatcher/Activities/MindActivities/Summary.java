package winmaster.mindwavewatcher.Activities.MindActivities;

import android.content.Intent;
import android.graphics.drawable.Drawable;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;
import java.util.Locale;
import io.realm.Realm;
import winmaster.mindwavewatcher.Activities.MainActivity;
import winmaster.mindwavewatcher.Helpers.Helpers;
import winmaster.mindwavewatcher.Models.ActivityResult;
import winmaster.mindwavewatcher.R;

public class Summary extends AppCompatActivity
{
    Realm realm;
    ActivityResult activityResult;

    TextView textViewActivityName;
    ImageView imageViewActivity;
    TextView textViewSettingLevelValue;
    TextView textViewSuccessValue;
    TextView textViewDurationValue;
    TextView textViewAvgAttentionValue;
    TextView textViewAvgMeditationValue;

    @Override
    protected void onCreate(Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_summary);

        textViewActivityName = findViewById(R.id.textViewActivityName);
        imageViewActivity = findViewById(R.id.imageViewActivity);
        textViewSettingLevelValue = findViewById(R.id.textViewSettingLevelValue);
        textViewSuccessValue = findViewById(R.id.textViewSuccessValue);
        textViewDurationValue = findViewById(R.id.textViewDurationValue);
        textViewAvgAttentionValue = findViewById(R.id.textViewAvgAttentionValue);
        textViewAvgMeditationValue = findViewById(R.id.textViewAvgMeditationValue);

        realm = Realm.getDefaultInstance();
        activityResult = realm.where(ActivityResult.class).equalTo("timestamp", getIntent().getExtras().getString(getResources().getString(R.string.TIMESTAMP))).findFirst();

        populateValuesToSummary();
    }

    private void populateValuesToSummary()
    {
        textViewActivityName.setText(activityResult.getActivityName());
        Drawable drawable = activityResult.getActivityName().equals(getString(R.string.attention)) ? getDrawable(R.drawable.attention) : getDrawable(R.drawable.meditation);
        imageViewActivity.setImageDrawable(drawable);
        textViewSettingLevelValue.setText(activityResult.getSettingLevel());
        textViewSuccessValue.setText(activityResult.isSuccess() ? getString(R.string.Yes) : getString(R.string.No));
        textViewDurationValue.setText(Helpers.getFormattedDurationTime(activityResult.getDuration()));
        textViewAvgAttentionValue.setText(String.format(Locale.getDefault(), "%.2f", activityResult.getAvgAttention()));
        textViewAvgMeditationValue.setText(String.format(Locale.getDefault(), "%.2f", activityResult.getAvgMeditation()));
    }

    public void buttonSummaryFinishClicked(View view)
    {
        startActivity(new Intent(this, MainActivity.class));
    }
}
