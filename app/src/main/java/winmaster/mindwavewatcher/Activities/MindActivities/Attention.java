package winmaster.mindwavewatcher.Activities.MindActivities;

import android.content.Intent;
import android.net.Uri;
import android.os.Handler;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import winmaster.mindwavewatcher.ColorArcProgressBar.ColorArcProgressBar;
import winmaster.mindwavewatcher.Helpers.SignalHelper;
import winmaster.mindwavewatcher.Logic.MindWaveLogic.MindWaveService;
import winmaster.mindwavewatcher.R;
import winmaster.mindwavewatcher.ViewModels.AttentionMeditationViewModel;

public class Attention extends AppCompatActivity
{
    AttentionMeditationViewModel viewModel;
    ColorArcProgressBar colorArcProgressBar;
    Button buttonAttentionGiveUp;
    Button buttonAttentionSummary;

    @Override
    protected void onCreate(final Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_attention);
        colorArcProgressBar = findViewById(R.id.progressBarAttention);
        buttonAttentionGiveUp = findViewById(R.id.buttonAttentionGiveUp);
        buttonAttentionSummary = findViewById(R.id.buttonAttentionSummary);

        viewModel = new AttentionMeditationViewModel(getResources().getString(R.string.attention), this);

        MindWaveService.currentAttention.observe(this, signal ->
        {
            viewModel.listAttention.add(signal.getValue());
            int value = SignalHelper.signalPercentageConverter(signal.getValue(), viewModel.maxThreshold);
            colorArcProgressBar.setCurrentValues(value);

            if(signal.getValue() >= viewModel.maxThreshold)
            {
                successAction();
            }
        });

        MindWaveService.currentMeditation.observe(this, signal -> viewModel.listMeditation.add(signal.getValue()));
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        MindWaveService.currentAttention.removeObservers(this);
        MindWaveService.currentMeditation.removeObservers(this);
    }

    private void successAction()
    {
        MindWaveService.currentAttention.removeObservers(this);
        MindWaveService.currentMeditation.removeObservers(this);

        viewModel.successAction();
        colorArcProgressBar.setCurrentValues(getResources().getInteger(R.integer.attentionMeterMaxValue));
        buttonAttentionGiveUp.setVisibility(View.INVISIBLE);

        new Handler().postDelayed(() ->
        {
            //open URI for triggered action
            startActivity(new Intent(Intent.ACTION_VIEW, Uri.parse(getResources().getString(R.string.searchUrl))));
            new Handler().postDelayed(() -> buttonAttentionSummary.setVisibility(View.VISIBLE), getResources().getInteger(R.integer.delayAction));
        }, getResources().getInteger(R.integer.delayAction));
    }

    public void buttonAttentionGiveUpClicked(View view)
    {
        MindWaveService.currentAttention.removeObservers(this);
        MindWaveService.currentMeditation.removeObservers(this);

        if(!viewModel.giveUpAction()){
            onBackPressed();
        }
    }

    public void buttonAttentionSummaryClicked(View view)
    {
        viewModel.summaryAction();
    }
}
