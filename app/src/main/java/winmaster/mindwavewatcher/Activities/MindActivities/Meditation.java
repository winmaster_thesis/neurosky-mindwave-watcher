package winmaster.mindwavewatcher.Activities.MindActivities;

import android.graphics.drawable.Drawable;
import android.os.Handler;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.DisplayMetrics;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;
import com.race604.drawable.wave.WaveDrawable;
import nl.dionsegijn.konfetti.KonfettiView;
import nl.dionsegijn.konfetti.models.Shape;
import nl.dionsegijn.konfetti.models.Size;
import winmaster.mindwavewatcher.Helpers.SignalHelper;
import winmaster.mindwavewatcher.Logic.MindWaveLogic.MindWaveService;
import winmaster.mindwavewatcher.R;
import winmaster.mindwavewatcher.ViewModels.AttentionMeditationViewModel;

public class Meditation extends AppCompatActivity
{
    AttentionMeditationViewModel viewModel;
    ImageView imageViewMeditationProgress;
    TextView textViewMeditationPercentage;
    Button buttonMeditationGiveUp;
    Button buttonMeditationSummary;
    Drawable mWaveDrawable;

    @Override
    protected void onCreate(Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_meditation);
        viewModel = new AttentionMeditationViewModel(getResources().getString(R.string.meditation), this);

        imageViewMeditationProgress = findViewById(R.id.imageViewMeditationProgress);
        textViewMeditationPercentage = findViewById(R.id.textViewMeditationPercentage);
        buttonMeditationGiveUp = findViewById(R.id.buttonMeditationGiveUp);
        buttonMeditationSummary = findViewById(R.id.buttonMeditationSummary);
        setmWaveDrawable();

        MindWaveService.currentMeditation.observe(this, signal ->
        {
            viewModel.listMeditation.add(signal.getValue());
            int value = SignalHelper.signalPercentageConverter(signal.getValue(), viewModel.maxThreshold);

            textViewMeditationPercentage.setText(String.format("%d%%", value));
            mWaveDrawable.setLevel((value * getResources().getInteger(R.integer.waveLevelMultiplier)));

            if(signal.getValue() >= viewModel.maxThreshold)
            {
                successAction();
            }
        });

        MindWaveService.currentAttention.observe(this, signal -> viewModel.listAttention.add(signal.getValue()));
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        MindWaveService.currentAttention.removeObservers(this);
        MindWaveService.currentMeditation.removeObservers(this);
    }

    private void setmWaveDrawable()
    {
        mWaveDrawable = new WaveDrawable(getDrawable(R.drawable.trophy_active));
        imageViewMeditationProgress.setImageDrawable(mWaveDrawable);
        ((WaveDrawable) mWaveDrawable).setWaveAmplitude(getResources().getInteger(R.integer.waveAmplitude));
        ((WaveDrawable) mWaveDrawable).setWaveLength(getResources().getInteger(R.integer.waveLength));
        ((WaveDrawable) mWaveDrawable).setWaveSpeed(getResources().getInteger(R.integer.waveSpeed));
    }

    private  void successAction()
    {
        MindWaveService.currentAttention.removeObservers(this);
        MindWaveService.currentMeditation.removeObservers(this);

        viewModel.successAction();
        mWaveDrawable.setLevel(getResources().getInteger(R.integer.meditationMeterMaxValue));
        textViewMeditationPercentage.setText(getResources().getString(R.string.hundredPercent));
        buttonMeditationGiveUp.setVisibility(View.INVISIBLE);

        new Handler().postDelayed(() ->
        {
            //Confetti animation
            setUpConfettiAndAnimations();

            new Handler().postDelayed(() -> buttonMeditationSummary.setVisibility(View.VISIBLE), getResources().getInteger(R.integer.delayAction));
        }, getResources().getInteger(R.integer.delayAction));
    }

    public void buttonMeditationGiveUpClicked(View view)
    {
        MindWaveService.currentAttention.removeObservers(this);
        MindWaveService.currentMeditation.removeObservers(this);

        if(!viewModel.giveUpAction()){
            onBackPressed();
        }
    }

    public void buttonMeditationSummaryClicked(View view)
    {
        viewModel.summaryAction();
    }

    private void setUpConfettiAndAnimations()
    {
        DisplayMetrics metrics = new DisplayMetrics();
        getWindowManager().getDefaultDisplay().getMetrics(metrics);

        KonfettiView viewConfetti = findViewById(R.id.viewKonfetti);
        viewConfetti.build()
            .addColors(getColor(R.color.confettiColor1), getColor(R.color.confettiColor2),
                    getColor(R.color.confettiColor3), getColor(R.color.confettiColor4),
                    getColor(R.color.confettiColor5))
            .setDirection(0.0, metrics.heightPixels/2)
            .setSpeed(1f, 5f)
            .setFadeOutEnabled(true)
            .setTimeToLive(20000L)
            .addShapes(Shape.RECT, Shape.CIRCLE)
            .addSizes(new Size(12,12))
            .setPosition(metrics.widthPixels / 2, metrics.heightPixels /5)
            .stream(100, 5000L);
    }
}
