package winmaster.mindwavewatcher.Activities;

import android.app.ActivityManager;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.support.annotation.NonNull;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;
import com.github.pwittchen.neurosky.library.message.enums.BrainWave;
import com.github.pwittchen.neurosky.library.message.enums.State;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Objects;
import java.util.stream.Collectors;
import io.realm.Realm;
import lecho.lib.hellocharts.model.Column;
import lecho.lib.hellocharts.model.ColumnChartData;
import lecho.lib.hellocharts.view.ColumnChartView;
import winmaster.mindwavewatcher.Activities.LoginActivities.LoginTestVariant;
import winmaster.mindwavewatcher.Activities.MindActivities.Attention;
import winmaster.mindwavewatcher.Enums.TestActivatorName;
import winmaster.mindwavewatcher.Helpers.ChartHelper;
import winmaster.mindwavewatcher.Enums.SettingsType;
import winmaster.mindwavewatcher.Helpers.ConnectivityHelper;
import winmaster.mindwavewatcher.Helpers.Helpers;
import winmaster.mindwavewatcher.Helpers.NotificationHelper;
import winmaster.mindwavewatcher.Helpers.SharedPreferencesUtils;
import winmaster.mindwavewatcher.Activities.MindActivities.Meditation;
import winmaster.mindwavewatcher.Helpers.SignalHelper;
import winmaster.mindwavewatcher.Logic.ExportLogic.ExportService;
import winmaster.mindwavewatcher.Logic.MindWaveLogic.MindWaveService;
import winmaster.mindwavewatcher.R;

public class MainActivity extends AppCompatActivity
{
    private TextView textViewAttention;
    private TextView textViewMeditation;
    private TextView textViewBlink;
    private TextView textViewState;
    private ImageView imageViewMindWave;

    private ColumnChartView chartEEG;
    private Button buttonConnect;
    private static long backPressed;

    @Override
    protected void onCreate(Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        textViewAttention = findViewById(R.id.textViewAttention);
        textViewMeditation = findViewById(R.id.textViewMeditation);
        textViewBlink = findViewById(R.id.textViewBlink);
        textViewState = findViewById(R.id.textViewState);
        imageViewMindWave = findViewById(R.id.imageViewMindWave);

        buttonConnect = findViewById(R.id.buttonConnect);
        chartEEG = findViewById(R.id.chartEEG);

        MindWaveService.currentAttention.observe(this, signal -> textViewAttention.setText(Helpers.getFormattedMessage("Attention: %d", signal)));
        MindWaveService.currentMeditation.observe(this, signal -> textViewMeditation.setText(Helpers.getFormattedMessage("Meditation: %d", signal)));
        MindWaveService.currentBlinkStrength.observe(this, signal -> textViewBlink.setText(Helpers.getFormattedMessage("Blink strength: %d", signal)));
        MindWaveService.currentSignalStrength.observe(this, signal -> imageViewMindWave.setImageDrawable(getDrawable(SignalHelper.getSignalStrengthDrawableId(signal))));

        MindWaveService.mindWaveState.observe(this, state -> {
            textViewState.setText(state.toString());
            if(state == State.CONNECTED){
                buttonConnect.setEnabled(false);
            }
            else buttonConnect.setEnabled(true);
        });

        MindWaveService.currentEEGWaves.observe(this, brainWaves ->
        {
            List<BrainWave> brainWavesSorted = brainWaves.stream().collect(Collectors.toList());
            Collections.sort(brainWavesSorted);

            List<Column> columns = new ArrayList<>();
            for (BrainWave brainWave : brainWavesSorted)
            {
                columns.add(ChartHelper.setChartColumn(brainWave.getValue(), brainWave.getType()));
            }
            ColumnChartData data = new ColumnChartData();
            data.setColumns(columns);
            chartEEG.setColumnChartData(data);
        });
    }

    @Override
    protected void onStart()
    {
        super.onStart();

        Realm.init(this);
        NotificationHelper.createNotificationChannel(this);
        manageFirstTimeOperations();
    }

    private void manageFirstTimeOperations()
    {
        boolean isUserFirstTime = SharedPreferencesUtils.readSharedBoolSetting(MainActivity.this, getResources().getString(R.string.PREF_USER_FIRST_TIME), true);

        if(isUserFirstTime)
        {
            SharedPreferencesUtils.saveSharedBoolSetting(this, getResources().getString(R.string.PREF_USER_FIRST_TIME), false);

            //saving default settings
            SharedPreferencesUtils.saveSharedStringSetting(this, getResources().getString(R.string.SETTINGS_TYPE), SettingsType.MEDIUM.name());

            //onboarding
            Intent openOnboarding= new Intent(this, OnboardingTutorial.class);
            openOnboarding.setFlags(Intent.FLAG_ACTIVITY_REORDER_TO_FRONT);
            startActivityIfNeeded(openOnboarding, 0);
        }
    }

    public boolean onCreateOptionsMenu(Menu menu)
    {
        super.onCreateOptionsMenu(menu);
        MenuInflater inflater = getMenuInflater();
        inflater.inflate(R.menu.main_menu, menu);
        return true;
    }

    public boolean onOptionsItemSelected(MenuItem item)
    {
        switch (item.getItemId())
        {
            case R.id.menuSettings:
            {
                startActivity(new Intent(this, Settings.class));
                return true;
            }
            case R.id.menuExportDatabase:
            {
                requestForPermissions();
                ExportService.exportAllDataToFiles(this);
                return true;
            }
            case R.id.menuClearDatabase:
            {
                getAlertClearData().show();
                return true;
            }
            case R.id.menuInfo:
            {
                startActivity(new Intent(this, OnboardingTutorial.class));
            }
            default:
                return super.onOptionsItemSelected(item);
        }
    }

    @Override
    public void onBackPressed()
    {
        Toast.makeText(getApplicationContext(), R.string.doubleBackPressToExit, Toast.LENGTH_SHORT).show();
        if (backPressed + R.integer.backPressedInterval > System.currentTimeMillis())
        {
            Intent intent = new Intent(Intent.ACTION_MAIN);
            intent.addCategory(Intent.CATEGORY_HOME);
            intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
            startActivity(intent);
        }
        else
            backPressed = System.currentTimeMillis();
    }

    private AlertDialog.Builder getAlertClearData()
    {
        return new AlertDialog.Builder(this)
            .setTitle(R.string.resetDataDialog)
            .setMessage(R.string.resetDataDescription)
            .setIcon(R.drawable.menu_clear_db)
            .setCancelable(true)
            .setPositiveButton(R.string.YesButton, (dialog, which) ->
            {
                SharedPreferencesUtils.resetSharedSettingsToDefault(getApplicationContext());
                ((ActivityManager)Objects.requireNonNull(getSystemService(ACTIVITY_SERVICE))).clearApplicationUserData();
            })
            .setNegativeButton(R.string.NoButton, (dialog, which) -> Toast.makeText(getApplicationContext(), getString(R.string.noReset), Toast.LENGTH_SHORT).show());
    }

    public void buttonConnectClicked(View view)
    {
       try {
           MindWaveService.getInstance().connect();
        } catch (Exception e){
            Toast.makeText(this, e.getMessage(), Toast.LENGTH_SHORT).show();
        }
    }

    public void buttonAttentionClicked(View view)
    {
       openTestTutorial(TestActivatorName.Attention.name());
    }

    public void buttonMeditationClicked(View view)
    {
        openTestTutorial(TestActivatorName.Meditation.name());
    }

    public void buttonLoginSimulationClicked(View view)
    {
        openTestTutorial(TestActivatorName.LoginSimulation.name());
    }

    private void openTestTutorial(String activatorName)
    {
       if(ConnectivityHelper.isDevicePreparedForTests(this)){
           startActivity(new Intent(this, TestExplanation.class)
                   .putExtra(getResources().getString(R.string.ACTIVATOR), activatorName));
       }
    }

    private void requestForPermissions()
    {
        if (ContextCompat.checkSelfPermission(this, android.Manifest.permission.READ_EXTERNAL_STORAGE) == PackageManager.PERMISSION_GRANTED)
            return;
        if (ContextCompat.checkSelfPermission(this, android.Manifest.permission.WRITE_EXTERNAL_STORAGE) == PackageManager.PERMISSION_GRANTED)
            return;

        ActivityCompat.requestPermissions(this, new String[]
        {
                android.Manifest.permission.READ_EXTERNAL_STORAGE,
                android.Manifest.permission.WRITE_EXTERNAL_STORAGE,
        }, getResources().getInteger(R.integer.PERMISSION_CODE));
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults)
    {
        if (requestCode == getResources().getInteger(R.integer.PERMISSION_CODE)) {
            //If permission is granted
            if (grantResults.length > 0 && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                //Displaying a toast
                Toast.makeText(this, getResources().getString(R.string.permissionsGranted), Toast.LENGTH_SHORT).show();
            } else {
                //Displaying another toast if permission is not granted
                Toast.makeText(this, getResources().getString(R.string.permissionsDenied), Toast.LENGTH_SHORT).show();
            }
        }
    }
}
