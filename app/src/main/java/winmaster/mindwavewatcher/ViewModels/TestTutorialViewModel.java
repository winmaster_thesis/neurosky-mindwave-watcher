package winmaster.mindwavewatcher.ViewModels;

import android.content.Context;
import android.content.Intent;
import winmaster.mindwavewatcher.Activities.LoginActivities.LoginTestVariant;
import winmaster.mindwavewatcher.Activities.MindActivities.Attention;
import winmaster.mindwavewatcher.Activities.MindActivities.Meditation;
import winmaster.mindwavewatcher.R;

public class TestTutorialViewModel
{
    private Context context;
    private String activator;

    private String testTitle;
    private String backgroundStory;
    private String testPurpose;
    private String testHowToBehave;
    private int testExplanationDrawableId;
    private Intent testIntent;

    public TestTutorialViewModel(Context ctx, String activatorName)
    {
        context = ctx;
        activator = activatorName;
        assignOtherValues();
    }

    public String getTestTitle() {
        return testTitle;
    }

    public String getBackgroundStory() {
        return backgroundStory;
    }

    public String getTestPurpose() {
        return testPurpose;
    }

    public String getTestHowToBehave() {
        return testHowToBehave;
    }

    public int getTestExplanation() {
        return testExplanationDrawableId;
    }

    public Intent getTestIntent() {
        return testIntent;
    }

    private void assignOtherValues()
    {
        switch(activator)
        {
            case "Attention":
            {
                setValuesToAttributes(
                        context.getResources().getString(R.string.attention),
                        context.getResources().getString(R.string.attentionStory),
                        context.getResources().getString(R.string.attentionPurpose),
                        context.getResources().getString(R.string.attentionHowToBehave),
                        R.drawable.attention_gif
                );
                testIntent = new Intent(context, Attention.class);
                break;
            }
            case "Meditation":
            {
                setValuesToAttributes(
                        context.getResources().getString(R.string.meditation),
                        context.getResources().getString(R.string.meditationStory),
                        context.getResources().getString(R.string.meditationPurpose),
                        context.getResources().getString(R.string.meditationHowToBehave),
                        R.drawable.meditation_gif
                );
                testIntent = new Intent(context, Meditation.class);
                break;
            }
            case "LoginSimulation":
            {
                setValuesToAttributes(
                        context.getResources().getString(R.string.loginSimulation),
                        context.getResources().getString(R.string.loginSimulationStory),
                        context.getResources().getString(R.string.loginSimulationPurpose),
                        context.getResources().getString(R.string.loginSimulationHowToBehave),
                        R.drawable.login_gif
                );
                testIntent = new Intent(context, LoginTestVariant.class);
                break;
            }
        }
    }

    private  void setValuesToAttributes(String title, String story, String purpose, String howTo, int explanation)
    {
        testTitle = title;
        backgroundStory = story;
        testPurpose = purpose;
        testHowToBehave = howTo;
        testExplanationDrawableId = explanation;
    }
}
