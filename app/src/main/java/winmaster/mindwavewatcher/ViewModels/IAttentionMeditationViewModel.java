package winmaster.mindwavewatcher.ViewModels;

public interface IAttentionMeditationViewModel
{
    void successAction();
    void setTimeSpan();
    boolean giveUpAction();
    void summaryAction();
}
