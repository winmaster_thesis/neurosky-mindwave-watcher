package winmaster.mindwavewatcher.ViewModels;

import android.content.Context;
import android.content.Intent;
import com.github.pwittchen.neurosky.library.message.enums.State;
import java.util.ArrayList;
import java.util.List;
import io.realm.Realm;
import winmaster.mindwavewatcher.Helpers.Helpers;
import winmaster.mindwavewatcher.Logic.MindWaveLogic.MindWaveService;
import winmaster.mindwavewatcher.Models.ActivityResult;
import winmaster.mindwavewatcher.Logic.SettingsLogic.SettingsService;
import winmaster.mindwavewatcher.Activities.MindActivities.Summary;
import winmaster.mindwavewatcher.R;

public class AttentionMeditationViewModel implements IAttentionMeditationViewModel
{
    private Realm realm;
    private Context context;
    private String className;
    private long timeSpan;
    private String timeStamp;
    private final String TIMESTAMP_EXTRA;

    public int maxThreshold;
    public List<Integer> listAttention;
    public List<Integer> listMeditation;

    public AttentionMeditationViewModel(String className, Context ctx)
    {
        realm = Realm.getDefaultInstance();
        this.className = className;
        listAttention = new ArrayList<>();
        listMeditation = new ArrayList<>();
        context = ctx;
        maxThreshold = className.equals(ctx.getResources().getString(R.string.attention))
                ? SettingsService.getAttentionForSetting(ctx) : SettingsService.getMeditationForSetting(ctx);
        timeSpan = System.currentTimeMillis();
        TIMESTAMP_EXTRA = ctx.getResources().getString(R.string.TIMESTAMP);
    }

    private void saveActivityResultDataToDb(boolean success)
    {
        timeStamp = Helpers.getCurrentTimeStampString();
        ActivityResult result = new ActivityResult();
        result.setTimestamp(timeStamp);
        result.setActivityName(className);
        result.setSettingLevel(SettingsService.getActualSettingType(context).name());
        result.setMaxThresholdValue(maxThreshold);
        result.setSuccess(success);
        result.setDuration(timeSpan);
        result.setAvgAttention(Helpers.getAverageFromListInt(listAttention));
        result.setAvgMeditation(Helpers.getAverageFromListInt(listMeditation));

        realm.beginTransaction();
        realm.insertOrUpdate(result);
        realm.commitTransaction();
    }

    @Override
    public void successAction()
    {
        setTimeSpan();
        if(MindWaveService.mindWaveState.getValue() == State.CONNECTED){
            saveActivityResultDataToDb(true);
        }
    }

    @Override
    public void setTimeSpan()
    {
        timeSpan = System.currentTimeMillis() - timeSpan;
    }

    @Override
    public boolean giveUpAction()
    {
        if(MindWaveService.mindWaveState.getValue() == State.CONNECTED)
        {
            setTimeSpan();
            saveActivityResultDataToDb(false);
            context.startActivity(new Intent(context, Summary.class).putExtra(TIMESTAMP_EXTRA, timeStamp));
            return true;
        }
        else return false;
    }

    @Override
    public void summaryAction()
    {
        context.startActivity(new Intent(context, Summary.class).putExtra(TIMESTAMP_EXTRA, timeStamp));
    }
}
