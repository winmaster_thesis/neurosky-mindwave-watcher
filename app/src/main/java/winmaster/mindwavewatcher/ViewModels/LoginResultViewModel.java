package winmaster.mindwavewatcher.ViewModels;

import android.content.Context;
import io.realm.Realm;
import winmaster.mindwavewatcher.Helpers.Helpers;
import winmaster.mindwavewatcher.Logic.SettingsLogic.SettingsService;
import winmaster.mindwavewatcher.Models.LoginResult;

public class LoginResultViewModel
{
    private Realm realm;
    private Context context;

    public LoginResultViewModel(Context ctx)
    {
        context = ctx;
        realm = Realm.getDefaultInstance();
    }

    public void saveLoginResultDataToDb(String username, String testVariant, boolean pictureOk, boolean verified, double percentage, String status)
    {
        LoginResult result = new LoginResult();
        result.setTimestamp(Helpers.getCurrentTimeStampString());
        result.setUsername(username);
        result.setTestVariant(testVariant);
        result.setPictureOK(pictureOk);
        result.setConfidenceLevelSetting(SettingsService.getLoginConfidenceLevel(context));
        result.setVerified(verified);
        result.setPercentage(percentage);
        result.setStatus(status);

        realm.beginTransaction();
        realm.insertOrUpdate(result);
        realm.commitTransaction();
    }
}
