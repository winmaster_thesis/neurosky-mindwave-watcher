package winmaster.mindwavewatcher.Logic.ExportLogic;

import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import android.media.MediaScannerConnection;
import android.net.Uri;
import android.os.Environment;
import android.support.v4.content.FileProvider;
import android.widget.Toast;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.OutputStream;
import java.util.ArrayList;
import java.util.List;
import io.realm.Realm;
import io.realm.RealmResults;
import winmaster.mindwavewatcher.Helpers.Helpers;
import winmaster.mindwavewatcher.Helpers.NotificationHelper;
import winmaster.mindwavewatcher.Models.ActivityResult;
import winmaster.mindwavewatcher.Models.LoginResult;
import winmaster.mindwavewatcher.R;

public abstract class ExportService
{
    private static List<File> files = new ArrayList<>();

    public static void exportAllDataToFiles(Context context)
    {
        String fileActivityResultsName = Helpers.getCurrentTimeStampShortString()+ "ActivityResults.csv";
        String fileLoginResultsName = Helpers.getCurrentTimeStampShortString() + "LoginTestsResults.csv";
        File fileActivityResults = new File(Environment.getExternalStoragePublicDirectory(Environment.DIRECTORY_DOWNLOADS), fileActivityResultsName);
        File fileLoginResults = new File(Environment.getExternalStoragePublicDirectory(Environment.DIRECTORY_DOWNLOADS), fileLoginResultsName);
        files.add(fileActivityResults);
        files.add(fileLoginResults);

        try {
            OutputStream osActivityResults = new FileOutputStream(fileActivityResults);
            osActivityResults.write((serializeActivityResults()).getBytes());
            osActivityResults.close();
            OutputStream osLoginResults = new FileOutputStream(fileLoginResults);
            osLoginResults.write((serializeLoginResults()).getBytes());
            osLoginResults.close();

            MediaScannerConnection.scanFile(context,
                    new String[] { fileActivityResults.toString(), fileLoginResults.toString() }, null,
                    (path1, uri) -> {});

            Toast.makeText(context, context.getResources().getString(R.string.exportFilesSuccess), Toast.LENGTH_SHORT).show();
            NotificationHelper.showNotification(context,
                    context.getResources().getString(R.string.exportNotificationTitle),
                    context.getResources().getString(R.string.exportNotificationContent),
                    getFileActionFromNotification(context, files),
                    context.getResources().getInteger(R.integer.exportNotification));

        } catch (IOException e) {
            Toast.makeText(context, e.getMessage(), Toast.LENGTH_SHORT).show();
        }
    }

    private static String serializeActivityResults()
    {
        Realm realm = Realm.getDefaultInstance();
        RealmResults<ActivityResult> results = realm.where(ActivityResult.class).findAll();
        StringBuilder stringBuilder = new StringBuilder();
        stringBuilder.append(ActivityResult.generateCsvTitleRow());
        stringBuilder.append("\n");

        for(ActivityResult result : results){
            stringBuilder.append(result.toString());
            stringBuilder.append("\n");
        }
        return stringBuilder.toString();
    }

    private static String serializeLoginResults()
    {
        Realm realm = Realm.getDefaultInstance();
        RealmResults<LoginResult> results = realm.where(LoginResult.class).findAll();
        StringBuilder stringBuilder = new StringBuilder();
        stringBuilder.append(LoginResult.generateCsvTitleRow());
        stringBuilder.append("\n");

        for(LoginResult result : results){
            stringBuilder.append(result.toString());
            stringBuilder.append("\n");
        }
        return stringBuilder.toString();
    }

    private static PendingIntent getFileActionFromNotification(Context context, List<File> attachmentFiles)
    {
        Intent emailIntent = new Intent(Intent.ACTION_SEND_MULTIPLE);
        emailIntent.setType("text/plan");
        emailIntent.putExtra(android.content.Intent.EXTRA_EMAIL, new String[] { "" });
        emailIntent.putExtra(android.content.Intent.EXTRA_SUBJECT, context.getResources().getString(R.string.emailSubject));
        emailIntent.putExtra(android.content.Intent.EXTRA_TEXT, context.getResources().getString(R.string.emailText));

        ArrayList<Uri> uris = new ArrayList<>();

        for (File file : attachmentFiles)
        {
            uris.add(FileProvider.getUriForFile(context, context.getApplicationContext().getPackageName() + ".provider", file));
        }
        emailIntent.putParcelableArrayListExtra(Intent.EXTRA_STREAM, uris);
        emailIntent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK | Intent.FLAG_GRANT_READ_URI_PERMISSION);
        return  PendingIntent.getActivity(context, 0, emailIntent, 0);
    }
}

