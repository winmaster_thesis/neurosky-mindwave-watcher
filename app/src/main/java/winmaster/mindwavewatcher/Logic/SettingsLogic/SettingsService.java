package winmaster.mindwavewatcher.Logic.SettingsLogic;

import android.content.Context;
import winmaster.mindwavewatcher.Enums.SettingsType;
import winmaster.mindwavewatcher.Helpers.SharedPreferencesUtils;
import winmaster.mindwavewatcher.R;

public abstract class SettingsService
{
    public static SettingsType getActualSettingType(Context ctx)
    {
        String setting = SharedPreferencesUtils.readSharedStringSetting(ctx, ctx.getString(R.string.SETTINGS_TYPE), SettingsType.MEDIUM.name());

        switch (setting)
        {
            case "EASY":
            {
                return SettingsType.EASY;
            }
            case "MEDIUM":
            {
                return SettingsType.MEDIUM;
            }
            case "HARD":
            {
                return SettingsType.HARD;
            }
            case "CUSTOM":
            {
                return SettingsType.CUSTOM;
            }
        }
        return SettingsType.MEDIUM;
    }

    public static int getAttentionForSetting(Context ctx)
    {
        SettingsType settingsType = getActualSettingType(ctx);
        return getAttentionForSetting(ctx, settingsType);
    }

    public static int getAttentionForSetting(Context ctx, SettingsType settingsType)
    {
        switch (settingsType)
        {
            case EASY:
            {
                return ctx.getResources().getInteger(R.integer.testsEasyAttentionValue);
            }
            case MEDIUM:
            {
                return ctx.getResources().getInteger(R.integer.testsMediumAttentionValue);
            }
            case HARD:
            {
                return ctx.getResources().getInteger(R.integer.testsHardAttentionValue);
            }
            case CUSTOM:
            {
                //If there is no proper value, use default medium setup
                return SharedPreferencesUtils.readSharedIntSetting(ctx, ctx.getResources().getString(R.string.SETTING_CUSTOM_ATTENTION), ctx.getResources().getInteger(R.integer.testsMediumAttentionValue));
            }
            default:
            {
                return ctx.getResources().getInteger(R.integer.testsMediumAttentionValue);
            }
        }
    }

    public static int getMeditationForSetting(Context ctx)
    {
        SettingsType settingsType = getActualSettingType(ctx);
        return getMeditationForSetting(ctx, settingsType);
    }

    public static int getMeditationForSetting(Context ctx, SettingsType settingsType)
    {
        switch (settingsType)
        {
            case EASY:
            {
                return ctx.getResources().getInteger(R.integer.testsEasyMeditationValue);
            }
            case MEDIUM:
            {
                return ctx.getResources().getInteger(R.integer.testsMediumMeditationValue);
            }
            case HARD:
            {
                return ctx.getResources().getInteger(R.integer.testsHardMeditationValue);
            }
            case CUSTOM:
            {
                //If there is no proper value, use default medium setup
                return SharedPreferencesUtils.readSharedIntSetting(ctx, ctx.getResources().getString(R.string.SETTING_CUSTOM_MEDITATION), ctx.getResources().getInteger(R.integer.testsMediumMeditationValue));
            }
            default:
            {
                return ctx.getResources().getInteger(R.integer.testsMediumMeditationValue);
            }
        }
    }

    public static int getLoginConfidenceLevel(Context ctx)
    {
        return SharedPreferencesUtils.readSharedIntSetting(ctx, ctx.getResources().getString(R.string.SETTING_CUSTOM_LOGIN_CONFIDENCE), ctx.getResources().getInteger(R.integer.loginConfidenceLevelDefault));
    }
}
