package winmaster.mindwavewatcher.Logic.MindWaveLogic;

import android.arch.lifecycle.MutableLiveData;
import android.support.annotation.NonNull;
import android.util.Log;
import com.github.pwittchen.neurosky.library.NeuroSky;
import com.github.pwittchen.neurosky.library.listener.ExtendedDeviceMessageListener;
import com.github.pwittchen.neurosky.library.message.enums.BrainWave;
import com.github.pwittchen.neurosky.library.message.enums.Signal;
import com.github.pwittchen.neurosky.library.message.enums.State;
import java.util.Set;

public abstract class MindWaveService
{
    private final static String LOG_TAG = "NeuroSky";
    private static NeuroSky neuroSky;

    public static MutableLiveData<State> mindWaveState = new MutableLiveData<>();
    public static MutableLiveData<Signal> currentAttention = new MutableLiveData<>();
    public static MutableLiveData<Signal> currentMeditation = new MutableLiveData<>();
    public static MutableLiveData<Signal> currentBlinkStrength = new MutableLiveData<>();
    public static MutableLiveData<Signal> currentSignalStrength = new MutableLiveData<>();
    public static MutableLiveData<Set<BrainWave>> currentEEGWaves = new MutableLiveData<>();

    public static synchronized NeuroSky getInstance()
    {
        if (neuroSky == null)
        {
            neuroSky = createNeuroSky();
        }
        return neuroSky;
    }

    @NonNull
    private static NeuroSky createNeuroSky()
    {
        return new NeuroSky(new ExtendedDeviceMessageListener()
        {
            @Override public void onStateChange(State state)
            {
                handleStateChange(state);
            }

            @Override public void onSignalChange(Signal signal)
            {
                handleSignalChange(signal);
            }

            @Override public void onBrainWavesChange(Set<BrainWave> brainWaves)
            {
                handleBrainWavesChange(brainWaves);
            }
        });
    }

    private static void handleStateChange(final State state)
    {
        if (neuroSky != null && state.equals(State.CONNECTED))
        {
            neuroSky.startMonitoring();
        }
        if (neuroSky != null && state.equals(State.DISCONNECTED))
        {
            neuroSky.stopMonitoring();
        }

        mindWaveState.setValue(state);
        Log.d(LOG_TAG, state.toString());
    }

    private static void handleSignalChange(final Signal signal)
    {
        switch (signal)
        {
            case ATTENTION:
                currentAttention.setValue(signal);
                break;
            case MEDITATION:
                currentMeditation.setValue(signal);
                break;
            case BLINK:
                currentBlinkStrength.setValue(signal);
                break;
            case POOR_SIGNAL:
                currentSignalStrength.setValue(signal);
                break;

        }
        Log.d(LOG_TAG, String.format("%s: %d", signal.toString(), signal.getValue()));
    }

    private static void handleBrainWavesChange(final Set<BrainWave> brainWaves)
    {
        for (BrainWave brainWave : brainWaves)
        {
            Log.d(LOG_TAG, String.format("%s: %d", brainWave.toString(), brainWave.getValue()));
        }
        currentEEGWaves.setValue(brainWaves);
    }
}
