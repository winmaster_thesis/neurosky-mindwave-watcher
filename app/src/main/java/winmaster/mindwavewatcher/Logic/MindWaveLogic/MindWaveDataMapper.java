package winmaster.mindwavewatcher.Logic.MindWaveLogic;

import com.github.pwittchen.neurosky.library.message.enums.BrainWave;
import winmaster.mindwavewatcher.Models.API.MindWaveDTO;

public abstract class MindWaveDataMapper
{
    public static MindWaveDTO prepareDataFromEEG()
    {
        MindWaveDTO dto = new MindWaveDTO();
        if(MindWaveService.currentAttention.getValue() != null){
            dto.Attention = MindWaveService.currentAttention.getValue().getValue();
        }

        if(MindWaveService.currentMeditation.getValue() != null){
            dto.Meditation = MindWaveService.currentMeditation.getValue().getValue();
        }

        if(MindWaveService.currentBlinkStrength.getValue() != null){
            dto.BlinkStrength = MindWaveService.currentBlinkStrength.getValue().getValue();
        }

        if(MindWaveService.currentEEGWaves.getValue() != null){
            for (BrainWave brainWave: MindWaveService.currentEEGWaves.getValue())
            {
                switch (brainWave)
                {
                    case DELTA:
                    {
                        dto.Delta = brainWave.getValue();
                        break;
                    }
                    case THETA:
                    {
                        dto.Theta = brainWave.getValue();
                        break;
                    }
                    case HIGH_ALPHA:
                    {
                        dto.Alpha_high = brainWave.getValue();
                        break;
                    }
                    case LOW_ALPHA:
                    {
                        dto.Alpha_low = brainWave.getValue();
                        break;
                    }
                    case HIGH_BETA:
                    {
                        dto.Beta_high = brainWave.getValue();
                        break;
                    }
                    case LOW_BETA:
                    {
                        dto.Beta_low = brainWave.getValue();
                        break;
                    }
                    case MID_GAMMA:
                    {
                        dto.Gamma_high = brainWave.getValue();
                        break;
                    }
                    case LOW_GAMMA:
                    {
                        dto.Gamma_low = brainWave.getValue();
                        break;
                    }
                }
            }
        }

        return dto;
    }
}
