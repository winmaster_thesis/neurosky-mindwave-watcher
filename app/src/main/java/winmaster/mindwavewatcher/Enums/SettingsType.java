package winmaster.mindwavewatcher.Enums;

public enum SettingsType
{
    EASY,
    MEDIUM,
    HARD,
    CUSTOM
}