package winmaster.mindwavewatcher.Enums;

public enum TestActivatorName
{
    Attention,
    Meditation,
    LoginSimulation
}
