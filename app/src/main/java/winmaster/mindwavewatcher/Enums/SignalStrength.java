package winmaster.mindwavewatcher.Enums;

public enum SignalStrength
{
    GOOD,
    MEDIUM,
    BAD
}
