package winmaster.mindwavewatcher.Models;

import java.util.Locale;
import io.realm.RealmObject;

public class LoginResult extends RealmObject
{
    //Timestamp type to string
    private String timestamp;

    //Username
    private String username;

    //Enum to string -> VariantA/VariantB
    private String testVariant;

    //True if picture is a valid chosen picture, false otherwise
    private boolean pictureOK;

    //get from certain level setup
    private int confidenceLevelSetting;

    //True if ML.NET classifier result is higher or equal Login Confidence level from Settings
    private boolean verified;

    //ML.NET classifier value;
    private double percentage;

    //Enum to string -> passed/not_passed/hackerman
    private String status;

    public LoginResult()
    {

    }

    public LoginResult(String timestamp, String username, String testVariant, boolean pictureOK, int confidenceLevelSetting, boolean verified, double percentage, String status)
    {
        this.timestamp = timestamp;
        this.username = username;
        this.testVariant = testVariant;
        this.pictureOK = pictureOK;
        this.confidenceLevelSetting = confidenceLevelSetting;
        this.verified = verified;
        this.percentage = percentage;
        this.status = status;
    }

    public String getTimestamp() {
        return timestamp;
    }

    public void setTimestamp(String timestamp) {
        this.timestamp = timestamp;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getTestVariant() {
        return testVariant;
    }

    public void setTestVariant(String testVariant) {
        this.testVariant = testVariant;
    }

    public boolean isPictureOK() {
        return pictureOK;
    }

    public void setPictureOK(boolean pictureOK) {
        this.pictureOK = pictureOK;
    }

    public boolean isVerified() {
        return verified;
    }

    public void setVerified(boolean verified) {
        this.verified = verified;
    }

    public int getConfidenceLevelSetting() {
        return confidenceLevelSetting;
    }

    public void setConfidenceLevelSetting(int confidenceLevelSetting) {
        this.confidenceLevelSetting = confidenceLevelSetting;
    }

    public double getPercentage() {
        return percentage;
    }

    public void setPercentage(double percentage) {
        this.percentage = percentage;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    @Override
    public String toString()
    {
        return String.format(Locale.getDefault(),"%s;%s;%s;%s;%d;%s;%.2f;%s", timestamp, username, testVariant, pictureOK, confidenceLevelSetting, verified, percentage, status);
    }

    public static String generateCsvTitleRow()
    {
        return String.format(Locale.getDefault(),"%s;%s;%s;%s;%s;%s;%s;%s", "timestamp", "username", "testVariant", "pictureMatched", "setting_confidenceLevel", "api_verified",  "api_percentage", "system_status");
    }
}
