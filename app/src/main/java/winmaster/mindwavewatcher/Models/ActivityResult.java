package winmaster.mindwavewatcher.Models;

import java.util.Locale;
import io.realm.RealmObject;

public class ActivityResult extends RealmObject
{
    // Timestamp type to string
    private String timestamp;

    //Attention or Meditation
    private String activityName;

    //Enum to string -> easy/medium/hard/custom
    private String settingLevel;

    //get from certain level setup
    private int maxThresholdValue;

    //True if success, false otherwise
    private boolean success;

    //how long test lasts in ms
    private long duration;

    //average attention level during test
    private double avgAttention;

    //average meditation level during test
    private double avgMeditation;


    public ActivityResult()
    {

    }

    public ActivityResult(String timestamp, String activityName, String settingLevel, int maxThresholdValue, boolean success, long duration, double avgAttention, double avgMeditation) {
        this.timestamp = timestamp;
        this.activityName = activityName;
        this.settingLevel = settingLevel;
        this.maxThresholdValue = maxThresholdValue;
        this.success = success;
        this.duration = duration;
        this.avgAttention = avgAttention;
        this.avgMeditation = avgMeditation;
    }

    public String getTimestamp() {
        return timestamp;
    }

    public void setTimestamp(String timestamp) {
        this.timestamp = timestamp;
    }

    public String getActivityName() {
        return activityName;
    }

    public void setActivityName(String activityName) {
        this.activityName = activityName;
    }

    public String getSettingLevel() {
        return settingLevel;
    }

    public void setSettingLevel(String settingLevel) {
        this.settingLevel = settingLevel;
    }

    public int getMaxThresholdValue() {
        return maxThresholdValue;
    }

    public void setMaxThresholdValue(int maxThresholdValue) {
        this.maxThresholdValue = maxThresholdValue;
    }

    public boolean isSuccess() {
        return success;
    }

    public void setSuccess(boolean success) {
        this.success = success;
    }

    public long getDuration() {
        return duration;
    }

    public void setDuration(long duration) {
        this.duration = duration;
    }

    public double getAvgAttention() {
        return avgAttention;
    }

    public void setAvgAttention(double avgAttention) {
        this.avgAttention = avgAttention;
    }

    public double getAvgMeditation() {
        return avgMeditation;
    }

    public void setAvgMeditation(double avgMeditation) {
        this.avgMeditation = avgMeditation;
    }


    @Override
    public String toString()
    {
        return String.format(Locale.getDefault(),"%s;%s;%s;%d;%s;%d;%.2f;%.2f", timestamp, activityName, settingLevel, maxThresholdValue, success, duration, avgAttention, avgMeditation);
    }

    public static String generateCsvTitleRow()
    {
        return String.format(Locale.getDefault(),"%s;%s;%s;%s;%s;%s;%s;%s", "timestamp", "activityName", "settingLevel", "maxThresholdValue", "success", "duration", "avgAttention", "avgMeditation");
    }
}
