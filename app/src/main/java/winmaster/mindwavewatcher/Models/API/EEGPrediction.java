package winmaster.mindwavewatcher.Models.API;

public class EEGPrediction
{
    public boolean prediction;
    public float score;
    public float probability;
}
