package winmaster.mindwavewatcher.Models.API;

public class MindWaveDTO
{
    public int Attention = 0;
    public int Meditation = 0;
    public int BlinkStrength = 0;
    public int Alpha_low = 0;
    public int Alpha_high = 0;
    public int Beta_low = 0;
    public int Beta_high = 0;
    public int Gamma_low = 0;
    public int Gamma_high = 0;
    public int Theta = 0;
    public int Delta = 0;
}
