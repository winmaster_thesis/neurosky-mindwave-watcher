# MindWave Watcher

App for user's tests for my Master Thesis in Computer Science 2020. :mortar_board: <br />
This program uses a MindWave Mobile  brain-computer interface to steer with a mobile application in different ways.

User can perform 3 different tests:
* [x] Attention test :wink:
* [x] Meditation test :relieved:
* [x] Login simulation test :satisfied:

Each test can be performed with one of 3 embedded difficulty levels. Users can also adjust settings by adding their own difficulty level. <br />
Data are stored on app database powered by [`Realm.io`](https://realm.io) <br />
After testing procedure, results can be easily transferred via sharing them by e-mail. They're also stored on the device in the ***Documents*** folder. <br />

## Device description

> The MindWave Mobile 2 safely measures and outputs the EEG power spectrums (alpha waves, beta waves, etc), NeuroSky eSense meters (attention and meditation) and eye blinks. The device consists of a headset, an ear-clip, and a sensor arm. The headset’s reference and ground electrodes are on the ear clip and the EEG electrode is on the sensor arm, resting on the forehead above the eye (FP1 position). It uses a single AAA battery with 8 hours of battery life. <br /> <br />
> [Technical Specs, NeuroSky MindWave store site](https://store.neurosky.com/pages/mindwave)

| NeuroSky MindWave Mobile 2 device |
| ------ |
| ![device](https://www.researchgate.net/profile/Phakkharawat_Sittiprapaporn/publication/318284067/figure/fig1/AS:513765775822848@1499502714788/MindWave-Mobile-Electroencephalography-Device-Neurosky-Inc.png) |


*  [Find out more about device](https://store.neurosky.com/pages/mindwave)
*  [MindWave Support developer program](http://developer.neurosky.com/docs/doku.php?id=mindwave)

# App download

App will not be uploaded to Google Play Store due to a high level of customization for certain problems connected to my scientific research. It also requires a certain device - MindWave Mobile.
But for scientific purposes, apk file can be downloaded here:

:floppy_disk: [`APK file download (university version)`](https://gitlab.com/winmaster_thesis/neurosky-mindwave-watcher/-/blob/master/apk/MindWaveWatcher_university.apk)

:floppy_disk: [`APK file download (basic)`](https://gitlab.com/winmaster_thesis/neurosky-mindwave-watcher/-/blob/master/apk/MindWaveWatcher.apk)


# Screens

| Splash screen | Onboarding | Main menu |
| ------ | ------ | ------ |
| ![](/screens/screen1.jpg) | ![](/screens/screen4.jpg) | ![](/screens/screen2.jpg) |

## Attention test

![attention gif](/screens/attention.gif)

| Test description | Test activity | Test results |
| ------ | ------ | ------ |
| ![](/screens/screen5.jpg) | ![](/screens/screen6.jpg) | ![](/screens/screen7.jpg) |

## Meditation test

![meditation gif](/screens/meditation.gif)

| How to adjust settings | Test Activity in progress | Test activity finished |
| ------ | ------ | ------ |
| ![](/screens/screen3.jpg) | ![](/screens/screen8.jpg) | ![](/screens/screen9.jpg) |

## Login simulation test

![login gif](/screens/login.gif)

| Test instruction | Test Version A | Test Version B |
| ------ | ------ | ------ |
| ![](/screens/screen12.jpg) | ![](/screens/screen10.jpg) | ![](/screens/screen11.jpg) |

### Login simulation results
There are 3 different possibilities due to the test variant. 

| Fail - when picture not matched, <br /> classifier verification not passed | Pass - when picture matched and classifier verification is ok <br /> (with knowledge of picture) | Suspicious - when picture matched by luck <br /> and classifier verification passed by luck <br /> (without knowledge of real picture) |
| ------ | ------ | ------ |
| ![](/screens/screen13.jpg) | ![](/screens/screen14.jpg) | ![](/screens/screen15.jpg) |

### Classifier:
> The classifier used for this project is checking whether login activity for the user is well-known like process, routine, or completely new, surprising test. <br />
> See my API project for more details: [`Mind Wave Classifier API`](https://gitlab.com/winmaster_thesis/neurosky-mindwave-classifier)

# Credits

Special thanks to authors of all plugins, libraries, icons and graphics, that I used in my project. It  made my app so beautiful. <br />
See [CREDITS](/CREDITS.md) file for details.
