# Iconsets and graphics

## Iconfinder
### icons 
> [www.iconfinder.com](https://www.iconfinder.com) <br />
> Free for commercial use (Include link to authors website)
> https://creativecommons.org/licenses/by/3.0/

### competition_prize_reward_success_trophy_win_winning_icon
[www.iconfinder.com/icons/514944/competition_prize_reward_success_trophy_win_winning_icon](https://www.iconfinder.com/icons/514944/competition_prize_reward_success_trophy_win_winning_icon)
> Free for commercial use (Include link to authors website)
> https://www.iconfinder.com/pareshdhake

## Flaticon
> <div>Icons made by <a href="https://www.flaticon.com/authors/vectors-market" title="Vectors Market">Vectors Market</a> from <a href="https://www.flaticon.com/" title="Flaticon">www.flaticon.com</a></div>

## Freepik
> [www.freepik.com](https://www.freepik.com/) <br />
> <a href="https://www.freepik.com/free-photos-vectors/business">Business vector created by macrovector_official - www.freepik.com</a>

## Neurosky MindWave icons from userGuide and official sites
> [developer.neurosky.com](http://developer.neurosky.com) <br />
> [http://download.neurosky.com/support_page_files/MindWaveMobile/docs/mindwave_mobile_user_guide.pdf](http://download.neurosky.com/support_page_files/MindWaveMobile/docs/mindwave_mobile_user_guide.pdf)


# Libaries

### AppIntro
[github.com/PaoloRotolo/AppIntro](https://github.com/PaoloRotolo/AppIntro)
> Licensed under the Apache License, Version 2.0 (the "License");

### Konfetti
[github.com/DanielMartinus/Konfetti](https://github.com/DanielMartinus/Konfetti)
> ISC License
> 
> Copyright (c) 2017 Dion Segijn
> 
> Permission to use, copy, modify, and/or distribute this software for any
> purpose with or without fee is hereby granted, provided that the above
> copyright notice and this permission notice appear in all copies.
> 
> THE SOFTWARE IS PROVIDED "AS IS" AND THE AUTHOR DISCLAIMS ALL WARRANTIES
> WITH REGARD TO THIS SOFTWARE INCLUDING ALL IMPLIED WARRANTIES OF
> MERCHANTABILITY AND FITNESS. IN NO EVENT SHALL THE AUTHOR BE LIABLE FOR
> ANY SPECIAL, DIRECT, INDIRECT, OR CONSEQUENTIAL DAMAGES OR ANY DAMAGES
> WHATSOEVER RESULTING FROM LOSS OF USE, DATA OR PROFITS, WHETHER IN AN
> ACTION OF CONTRACT, NEGLIGENCE OR OTHER TORTIOUS ACTION, ARISING OUT OF
> OR IN CONNECTION WITH THE USE OR PERFORMANCE OF THIS SOFTWARE.


### Neurosky Android SDK by Piotr Wittchen :heart:
[github.com/pwittchen/neurosky-android-sdk](https://github.com/pwittchen/neurosky-android-sdk)
> Copyright 2018 Piotr Wittchen
> 
> Licensed under the Apache License, Version 2.0 (the "License");
> you may not use this file except in compliance with the License.
> You may obtain a copy of the License at
> 
>    http://www.apache.org/licenses/LICENSE-2.0
> 
> Unless required by applicable law or agreed to in writing, software
> distributed under the License is distributed on an "AS IS" BASIS,
> WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
> See the License for the specific language governing permissions and
> limitations under the License.

### Color Arc ProgressBar
[github.com/Shinelw/ColorArcProgressBar](https://github.com/Shinelw/ColorArcProgressBar)

### Multi ProgressBar
[github.com/knight-rider1609/MultiProgressBar](https://github.com/knight-rider1609/MultiProgressBar)

> Copyright 2019 Aseem Khare
> 
> Licensed under the Apache License, Version 2.0 (the "License");
> you may not use this file except in compliance with the License.
> You may obtain a copy of the License at
> 
>    http://www.apache.org/licenses/LICENSE-2.0
> 
> Unless required by applicable law or agreed to in writing, software
> distributed under the License is distributed on an "AS IS" BASIS,
> WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
> See the License for the specific language governing permissions and
> limitations under the License.

### HelloCharts
[github.com/lecho/hellocharts-android](https://github.com/lecho/hellocharts-android)
> HelloCharts	
> Copyright 2014 Leszek Wach
> 
> Licensed under the Apache License, Version 2.0 (the "License");
> you may not use this file except in compliance with the License.
> You may obtain a copy of the License at
> 
>    http://www.apache.org/licenses/LICENSE-2.0
> 
> Unless required by applicable law or agreed to in writing, software
> distributed under the License is distributed on an "AS IS" BASIS,
> WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
> See the License for the specific language governing permissions and
> limitations under the License.

### WaveLoading
[github.com/race604/WaveLoading](https://github.com/race604/WaveLoading)
> MIT License

### Android Gif Drawable by Droids on Roids :heart:
[github.com/koral--/android-gif-drawable](https://github.com/koral--/android-gif-drawable)
> MIT License

### Picasso
[github.com/square/picasso](https://github.com/square/picasso)
> Copyright 2013 Square, Inc.
> 
> Licensed under the Apache License, Version 2.0 (the "License");
> you may not use this file except in compliance with the License.
> You may obtain a copy of the License at
> 
>    http://www.apache.org/licenses/LICENSE-2.0
> 
> Unless required by applicable law or agreed to in writing, software
> distributed under the License is distributed on an "AS IS" BASIS,
> WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
> See the License for the specific language governing permissions and
> limitations under the License.